# Pokedex

## What Is About?
<p>A mobile aplication that that allows the user to check information about pokemons and add data and curiosities about them.</p>

## For Whom Is It Destined?
<p>For every one that wants to save information about pokemons and easily check this information.</p>

## Functions
<ul>
  <li>The user can create an account.</li>
  <li>The user can add a pokemon to his account.</li>
  <li>The user can add and check information about pokemon added. </li>
</ul>

## Developers
Gabrielle Ribeiro Gomes  
Gustavo Afonso Pires Severo  
